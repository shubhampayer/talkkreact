import React, { Component } from 'react'
import {Nav,Image, Navbar, Dropdown, Container,ListGroup, Card } from 'react-bootstrap';
import { faAngleDown,faBell, faUsers, faDesktop, faComments,faRobot, faTicketAlt, faBullhorn, faUserPlus, faChartLine  } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {Link} from "react-router-dom";
import logoBubble from '../../assets/images/logo-bubble.png';
import { Scrollbars } from 'react-custom-scrollbars';

export default class header2 extends Component {
    render() {
      return (
            <>
                <div class="sidebar-overlay"></div>
                <div class="app-sidebar-nav">
                    <div class="app-logo">
                        <Card.Link as={Link} class="logo-link">
                            <Image src={logoBubble} alt="Talkk Bubble Logo" class="img-responsive"/>
                        </Card.Link>
                    </div>
                    <nav class="app-navigation">
                        <Scrollbars class="navigation-scrollable" autoHide autoHideTimeout={1000} autoHideDuration={200} >
                            <ul class="nav menu">
                                <li class="menu-item">
                                    <a href="/admin/dashboard" class="menu-link">
                                        <FontAwesomeIcon icon={faDesktop} className="faIcons" />
                                        <span class="menu-title">Dashboard</span>
                                    </a>
                                </li>
                                <li class="menu-item  has-submenu-item">
                                    <a class="menu-link toggle-sub-menu" href="#sub-menu7" data-toggle="collapse">
                                        <FontAwesomeIcon icon={faUsers} className="faIcons" />
                                        <span class="menu-title">Users</span>
                                    </a>
                                    <div class="collapse collapsed-sub-menu">
                                        <ul>
                                            <li>
                                                <a href="/admin/users/your-database">Your Database</a>
                                            </li>
                                            <li>
                                                <a href="/admin/users/other-users">Users</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-item <%= (requestPath=='/admin/chats') ? 'app-menu-active':'' %>">
                                    <a href="/admin/chats" class="menu-link">
                                        <FontAwesomeIcon icon={faComments} className="faIcons" />
                                        <span class="menu-title">Chats</span>
                                    </a>
                                </li>
                                <li class="menu-item <%= (requestPath=='/admin/feedback') ? 'app-menu-active':'' %>">
                                    <a href="/admin/feedback" class="menu-link">
                                        <FontAwesomeIcon icon={faComments} className="faIcons" />
                                        <span class="menu-title">Feedback</span>
                                    </a>
                                </li>
                                <li class="menu-item has-submenu-item <%= (requestPath=='/admin/notification') || (requestPath=='/admin/notification/whatsapp-template') || (requestPath=='/admin/notification') || (requestPath=='/admin/notification/setting') ? 'app-menu-active':'' %>">
                                    <a class="menu-link toggle-sub-menu" href="#sub-menu2" data-toggle="collapse">
                                        <FontAwesomeIcon icon={faBell} className="faIcons" />
                                        <span class="menu-title">Notification</span>
                                    </a>
                                    <div id="sub-menu2" class="collapse collapsed-sub-menu">
                                        <ul>
                                            <li class="<%= (requestPath=='/admin/notification/whatsapp-template') ? 'active':'' %>">
                                                <a href="/admin/notification/whatsapp-template">WhatsApp Templates</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/notification') ? 'active':'' %>">
                                                <a href="/admin/notification">Notification Templates</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/notification/setting') ? 'active':'' %>">
                                                <a href="/admin/notification/setting">Channels Settings</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-item">
                                    <a href="/admin/tickets" class="menu-link">
                                        {/* <i class="fi flaticon-ticket"></i> */}
                                        <FontAwesomeIcon icon={faTicketAlt} className="faIcons" />
                                        <span class="menu-title">Tickets</span>
                                    </a>
                                </li>
                                <li class="menu-item has-submenu-item">
                                    <a class="menu-link toggle-sub-menu" href="#sub-menu3" data-toggle="collapse">
                                        <FontAwesomeIcon icon={faRobot} className="faIcons"/>
                                        <span class="menu-title">Bots</span>
                                    </a>
                                    <div class="collapse collapsed-sub-menu">
                                        <ul>
                                            <li class="<%= (requestPath=='/admin/menu') ? 'active':'' %>">
                                                <a href="/admin/menu">Menu</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/intents') ? 'active':'' %>">
                                                <a href="/admin/intents">Intents</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/entities') ? 'active':'' %>">
                                                <a href="/admin/entities">Entities</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/categories') ? 'active':'' %>">
                                                <a href="/admin/categories">Categories</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/qna') ? 'active':'' %>">
                                                <a href="/admin/qna">QNA</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/small-talkk') ? 'active':'' %>">
                                                <a href="/admin/small-talkk">Small Talkk</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/default-response') ? 'active':'' %>">
                                                <a href="/admin/default-response">Response Templates</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-item has-submenu-item <%= (requestPath=='/admin/intents-lite') ? 'app-menu-active':'' %>">
                                    <a href="#sub-menu6" class="menu-link toggle-sub-menu" data-toggle="collapse">
                                        <FontAwesomeIcon icon={faRobot} className="faIcons"/>
                                        <span class="menu-title">Bot Kit</span>
                                    </a>
                                    <div id="sub-menu6" class="collapse collapsed-sub-menu">
                                        <ul>
                                            <li class="<%= (requestPath=='/admin/intents-lite') ? 'active':'' %>">
                                                <a href="/admin/intents-lite">Intents</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-item has-submenu-item <%= (requestPath=='/admin/marketing') || (requestPath=='/admin/broadcast') || (requestPath=='/admin/newsletter')? 'app-menu-active':'' %>">
                                    <a class="menu-link toggle-sub-menu" href="#sub-menu4" data-toggle="collapse">
                                        <FontAwesomeIcon icon={faBullhorn} className="faIcons"/>
                                        <span class="menu-title">Marketing</span>
                                    </a>
                                    <div id="sub-menu4" class="collapse collapsed-sub-menu">
                                        <ul>
                                            <li class="<%= (requestPath=='/admin/broadcast') ? 'active':'' %>">
                                                <a href="/admin/broadcast">Broadcast</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/newsletter') ? 'active':'' %>">
                                                <a href="/admin/newsletter">Newsletters</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-item has-submenu-item <%= (requestPath=='/admin/reports') || (requestPath=='/admin/reports/usecase-summary-v2') || (requestPath=='/admin/reports/leads') || (requestPath=='/admin/reports/user-statistics') || (requestPath=='/admin/reports/users-report') || (requestPath=='/admin/reports/sneak-conversation') || (requestPath=='/admin/reports/newsletter-subscriber') || (requestPath=='/admin/reports/missed-intent') || (requestPath=='/admin/reports/export-report') || (requestPath=='/admin/covid-registary') ? 'app-menu-active':'' %>">
                                    <a class="menu-link toggle-sub-menu" href="#sub-menu5" data-toggle="collapse">
                                        <FontAwesomeIcon icon={faChartLine} className="faIcons"/>
                                        <span class="menu-title">Reports</span>
                                    </a>
                                    <div id="sub-menu5" class="collapse collapsed-sub-menu">
                                        <ul>
                                            <li class="<%= (requestPath=='/admin/reports/usecase-summary-v2') ? 'active':'' %>">
                                                <a href="/admin/reports/usecase-summary-v2">Usecase Statistics</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/eports/user-statistics') ? 'active':'' %>">
                                                <a href="/admin/reports/user-statistics">User Statistics</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/reports/users-report') ? 'active':'' %>">
                                                <a href="/admin/reports/users-report">User</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/reports/sneak-conversation') ? 'active':'' %>">
                                                <a href="/admin/reports/sneak-conversation">Sneak Conversations</a>
                                            </li>

                                            <li class="<%= (requestPath=='/admin/reports/conversation-history') ? 'active':'' %>">
                                                <a href="/admin/reports/conversation-history">Conversation History</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/reports/leads') ? 'active':'' %>">
                                                <a href="/admin/reports/leads">Leads</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/reports/newsletter-subscriber') ? 'active':'' %>">
                                                <a href="/admin/reports/newsletter-subscriber">Newsletter Subscriber</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/reports/missed-intent') ? 'active':'' %>">
                                                <a href="/admin/reports/missed-intent">Missed Intents</a>
                                            </li>

                                            <li class="<%= (requestPath=='/admin/reports/export-report') ? 'active':'' %>">
                                                <a href="/admin/reports/export-report">Export Report</a>
                                            </li>
                                            <li class="<%= (requestPath=='/admin/covid-registary') ? 'active':'' %>">
                                                <a href="/admin/covid-registary">Covid Registary</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-item <%= (requestPath=='/admin/media') ? 'app-menu-active':'' %>">
                                    <a href="/admin/media" class="menu-link">
                                        <FontAwesomeIcon icon={faUserPlus} className="faIcons"/>
                                        <span class="menu-title">DMS</span>
                                    </a>
                                </li>
                                <li class="menu-item <%= (requestPath=='/admin/categories') ? 'app-menu-active':'' %>">
                                    <a href="/admin/categories" class="menu-link">
                                        <i class="fi flaticon-content" aria-hidden="true"></i>
                                        <span class="menu-title ">E-forms</span>
                                    </a>
                                </li>
                            </ul>
                        </Scrollbars>
                    </nav>
                </div>
            </>
        )
    }
}
