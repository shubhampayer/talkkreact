import React, { Component } from 'react'
import {Nav,Image, Navbar, Dropdown, Container,ListGroup } from 'react-bootstrap';
import { faAngleDown,faBell, faCog, faEllipsisH,faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { faUserCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Absalogo from "../../assets/images/absa_logo.svg";
import {Link} from "react-router-dom";
import Routes from "../../routes"

export default class header2 extends Component {
    render() {
      const Title= "Bot";
      return (
            <>
                  <Navbar variant="dark" expanded>
      <Container fluid className="px-0">
        <div className="d-flex justify-content-between w-100 align-items-center">
          <Nav classnam="mr-auto">
              <span className="app-title">{Title}</span>
              <Dropdown as={Nav.Item} className="bot-list-dropdown">
              <Dropdown.Toggle as={Nav.Link} className="px-0">
                <div className="bot-list-icon d-flex align-items-center">
                  <FontAwesomeIcon icon={faEllipsisH} />
                </div>
              </Dropdown.Toggle>
              <Dropdown.Menu className="dropdown-menu-left">
                <ListGroup className="list-group-flush">
                  <input type="text" placeholder="Search Bots" class="mx-3 my-2 w-auto mt-3 form-control"/>
                  <span className="text-primary mx-3">Active Bots</span>
                  <Dropdown.Item>
                    FGLI
                  </Dropdown.Item>
                  <Dropdown.Item>
                    BMS
                  </Dropdown.Item>
                  <Dropdown.Item>
                    ABSA
                  </Dropdown.Item>
                  <Dropdown.Item>
                    Helpify
                  </Dropdown.Item>
                </ListGroup>
              </Dropdown.Menu>
            </Dropdown>
          </Nav>
          <Nav className="ml-auto">
          <Dropdown as={Nav.Item} className="notification-dropdown">
              <Dropdown.Toggle as={Nav.Link} className="px-0">
                <div className="notification d-flex align-items-center">
                  <FontAwesomeIcon icon={faBell} />
                  <span class="notification-counter">5</span>
                </div>
              </Dropdown.Toggle>
              <Dropdown.Menu className="dashboard-dropdown notifications-dropdown dropdown-menu-lg dropdown-menu-right">
                <div class="up-arrow"></div>
                <ListGroup className="list-group-flush">
                  <Nav.Link href="#" className="text-center text-primary fw-bold border-bottom border-light py-1">
                    Notifications
                  </Nav.Link>
                  <Dropdown.Item className="py-1 notification-message">
                    <span>Meeting Scheduled for document verification</span>
                    <span className="d-block text-small text-light">1 min ago</span>
                  </Dropdown.Item>
                  <Dropdown.Item className="py-1 notification-message">
                    <span>Meeting Scheduled for document verification</span>
                    <span className="d-block text-small text-light">1 min ago</span>
                  </Dropdown.Item>
                  <Dropdown.Item className="py-1 notification-message">
                    <span>Meeting Scheduled for document verification</span>
                    <span className="d-block text-small text-light">1 min ago</span>
                  </Dropdown.Item>
                  <Dropdown.Item className="py-1 notification-message">
                    <span>Meeting Scheduled for document verification</span>
                    <span className="d-block text-small text-light">1 min ago</span>
                  </Dropdown.Item>
                  <Dropdown.Item className="text-center text-primary fw-bold py-1 border-top pt-2">
                    View all
                  </Dropdown.Item>
                </ListGroup>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown as={Nav.Item}>
              <Dropdown.Toggle as={Nav.Link} className="px-0">
                <div className="menu-bot-name d-flex align-items-center">
                    <span>AB</span>
                  <FontAwesomeIcon icon={faAngleDown} className="ml-2" />
                </div>
              </Dropdown.Toggle>
              <Dropdown.Menu className="dropdown-menu-right">
                <div class="up-arrow"></div>
                <Image src={Absalogo} className="absa-logo" alt="absa logo"/>
                <div className="bot-info">
                    <strong>Absa</strong>
                    <span className="d-block font-small">customer.contact@absa.africa</span>
                </div>
                <Dropdown.Item className="fw-bold">
                  <FontAwesomeIcon icon={faUserCircle} className="mr-2" /> My Account
                </Dropdown.Item>
                <Dropdown.Item className="fw-bold">
                  <FontAwesomeIcon icon={faCog} className="mr-2" /> Settings
                </Dropdown.Item>
                <Dropdown.Item className="fw-bold" as={Link} to={Routes.Login.path}>
                  <FontAwesomeIcon icon={faSignOutAlt} className="mr-2" /> Logout
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Nav>
        </div>
      </Container>
    </Navbar>
 
            </>
        )
    }
}
