import React, { Component } from 'react'
import logo from '../../../assets/images/talkklogo.svg';
import { Routes } from "../../../routes";
import {Card, Image, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default class home extends Component {
    constructor(props) {
        super(props);
        this.state = {
          username: "admin@yopmail.com",
          password: "matrix",
          user: "",
          pass: "",
          errorsusername: "",
          errorspassword: "",
          showingAlert: false
        };
    }
    handleEmailChange(e) {
        this.setState({user: e.target.value});
    }
    handlePassChange(e){
        this.setState({pass: e.target.value});
    }
    handleLoginSubmit(e) {
        let user1= this.state.username;
        let user2= this.state.user;
        let pass1= this.state.password;
        let pass2= this.state.pass;
        e.preventDefault();
        if(user1 == user2 && pass1 == pass2){
            this.props.history.push(Routes.Botlist.path);
        }
        else if(user1 == user2 && pass2 == ""){
            this.setState({
                errorspassword: "password cannot be empty",
                errorsusername: "",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
              }, 2000);
        }
        else if(user2 == "" && pass2 == pass1){
            this.setState({
                errorsusername: "Email cannot be empty",
                errorspassword: "",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
              }, 2000);
        }
        else if(user2 == ""){
            this.setState({
                errorsusername: "Email cannot be empty",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
              }, 2000);
            
        }
        else if(pass2 == ""){
                this.setState({errorspassword: "Password cannot be empty", showingAlert: true})
                setTimeout(() => {
                    this.setState({
                      showingAlert: false
                    });
                  }, 2000);
        }
        else if(user1 !== user2 && user2 !== ""){
            this.setState({
                errorsusername: "Enter valid email address",
                errorspassword: "",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
            }, 2000);
        }
        else if(pass1 !== pass2 && pass2 !== ""){
            this.setState({
                errorspassword: "Password is incorrect",
                errorsusername: "",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
              }, 2000);
        }
        else if(pass1 !== pass2 && user1!== user2){
            this.setState({
                errorsusername: "Email cannot be empty",
                errorspassword: "Password cannot be empty",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
              }, 2000);
        }
    }
    render() {
        return (
            <div className="app-auth">
                <div className="form-body">
                    <div className="auth-bg-white"></div>
                    <div className="auth-layout">
                        <section className="company-section">
                            <div className="form-wrapper">
                                <Card.Link as={Link} to={Routes.Login.path} className="website-logo">
                                  <Image className="logo-size" src={logo} width="170" alt="Talkk Logo" />
                                </Card.Link>
                                <Form onSubmit={this.handleLoginSubmit.bind(this)}>
                                    <div id="status"></div>
                                    <Form.Group>
                                        <Form.Label>Enter your Email Address</Form.Label>
                                        <Form.Label className="input-wrapper input-floatable">
                                            <Form.Control type="text" name="username" placeholder="Email Address" autoComplete="off" onChange={this.handleEmailChange.bind(this)}/>
                                            <span className="input-label">Email Address</span>
                                        </Form.Label>
                                        <div className={`error-text ${this.state.showingAlert ? 'alert-shown' : 'error-hidden'}`}>{this.state.errorsusername}</div>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Enter your Password</Form.Label>
                                        <Form.Label className="input-wrapper input-password input-floatable">
                                            <i  className="wc-ri fa fa-eye-slash"></i>
                                            <Form.Control name="password" type="password" placeholder="Password"  autoComplete="off" onChange={this.handlePassChange.bind(this)} />
                                            <span className="input-label">Password</span>
                                        </Form.Label>
                                        <div className={`error-text ${this.state.showingAlert ? 'alert-shown' : 'error-hidden'}`}>{this.state.errorspassword}</div>
                                    </Form.Group>
                                    <div className="form-button">
                                        <Button type="submit" className="btn btn-orange mr-10">
                                            Login
                                        </Button>
                                        <Card.Link as={Link} to={Routes.Forgot.path}>Forgot password?</Card.Link>
                                    </div>
                                </Form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        )
    }
}
