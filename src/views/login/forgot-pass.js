import React, { Component } from 'react';
import logo from '../../assets/images/talkklogo.svg';
import Routes from '../../routes';
import {Card, Image, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default class forgotpass extends Component {
    render() {
        return (
          <div className="app-auth">
            <div className="form-body">
              <div className="auth-bg-white"></div>
              <div className="auth-layout">
                <section className="company-section">
                  <div className="form-wrapper">
                    <Card.Link as={Link} to={Routes.Login.path} className="website-logo">
                      <Image className="logo-size" src={logo} width="170" alt="Talkk Logo" />
                    </Card.Link>
                    <Form method="post">
                        <Form.Group>
                          <Form.Label className="input-wrapper input-floatable">
                            <Form.Control name="email" type="text" placeholder="Email Address" autofocus tabindex="1" autocomplete="off"/>
                            <span className="input-label">Email Address</span>
                          </Form.Label>
                        </Form.Group>
                        <div className="form-button">
                          <Button type="button" className="btn btn-orange btn-block">Send Reset Link</Button>
                          <div className="other-links">
                            <Card.Link as={Link} to={Routes.Login.path} className="back-links">
                              <i className="fa fa-angle-double-left"></i>
                              back to login
                            </Card.Link>
                          </div>
                        </div>
                      </Form>
                  </div>
                </section>
              </div>
            </div>
          </div>
        )
    }
}
