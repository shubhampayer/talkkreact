import React, { Component } from 'react'
import { Card, Form, Image, Button } from 'react-bootstrap';
import logo from '../../assets/images/talkklogo.svg';
import { Routes } from "../../routes";
import { Link } from 'react-router-dom';


export default class companycode extends Component {
    constructor(props) {
        super(props);
        this.state = {
          companycode: "",
          errorscompany: "",
          showingAlert: false
        };
    }
    handleCnameChange(e) {
        this.setState({companycode: e.target.value});
    }
    handleSubmit(e) {
        e.preventDefault();
        if(this.state.companycode.length>2){
            this.props.history.push(Routes.Login.path);
        }
        else{
            this.setState({
                errorscompany: "Company code cannot be empty",
                showingAlert: true
            })
            setTimeout(() => {
                this.setState({
                  showingAlert: false
                });
            }, 2000);
        }
    }
    render() {
        return (
            <div className="app-auth">
                <div className="form-body">
                    <div className="auth-bg-white"></div>
                    <div className="auth-layout">
                        <section className="company-section">
                            <div className="form-wrapper">
                                <Card.Link as={Link} to={Routes.CompanyCode.path} className="website-logo">
                                  <Image className="logo-size" src={logo} width="170" alt="Talkk Logo" />
                                </Card.Link>
                                <Form  autocomplete="off" onSubmit={this.handleSubmit.bind(this)}>
                                    <div className="form-group mb-10">
                                        <Form.Label>Enter your Company Code</Form.Label>
                                        <Form.Label className="input-wrapper input-floatable">
                                            <Form.Control type="text" placeholder="Company Code" autofocus onChange={this.handleCnameChange.bind(this)}/>
                                            <span className="input-label">Company Code</span>
                                        </Form.Label>
                                        <div className={`error-text ${this.state.showingAlert ? 'alert-shown' : 'error-hidden'}`}>{this.state.errorscompany}</div>
                                    </div>
                                    <div className="form-button">
                                        <Button type="submit" className="btn btn-orange">
                                            Proceed
                                        </Button>
                                    </div>
                                </Form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        )
    }
}
