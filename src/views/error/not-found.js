import React, { Component } from 'react'
import NotFoundImg from "../../assets/images/404.svg";
export default class notfound extends Component {
    render() {
        return (
            <div className="not-found">
                <div className="container">
                    <div className="brokenPageHolder error">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="mt-30 mb-30 img-holder">
                                    <img src={NotFoundImg}/>
                                </div>
                                <h1 className="error-title">Oops! <br/> Something's wrong here...</h1>
                                <p>We can't find page you're looking for. Please Reach us or head back to home.</p>
                            </div>
                        </div>
                        <div className="row mt-20 mb-30">
                            <div className="col-md-12">
                                <a href="/bot-list" className="btn btn-primary btn-round">Go Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
