import Navbar from "../../views/components/header2";
import PeopleIcon from "../../assets/images/queue_management.svg";
import {Row,Col,Nav} from 'react-bootstrap';
import Sidebar from "../components/sidebar"

export default () =>{
        return (
            <div className="app-main">
                <div className="app-container wide">
                    <Sidebar />
                    <div className="app-layout-wrapper">
                    <Navbar />
                    <div className="app-layout">
                    <div className="custom-absa">
                        <Row>
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Dashboard</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Corporate Tv</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Absa CIB</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col> 
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Absa Wealth</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>                           
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Absa Retail</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Eagle Eye Dashboard</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Customer Meeting</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>
                            <Col md="2" sm="2" xs="3">
                                <a href="/home">
                                    <div className="setting-thumbs">
                                        <div className="setting-thumb-types">
                                            <img src={PeopleIcon} className="setting-type-img" alt="People SVG Icon" />
                                            <p className="setting-type-title">Meeting Analysis</p>
                                        </div>
                                    </div>
                                </a>                      
                            </Col>
                        </Row>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        )
}
