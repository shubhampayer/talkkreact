import React, { Component, useState } from 'react'
import Navbar from '../components/header2'
import {Row,Container,Col,Modal,Dropdown,Button } from 'react-bootstrap';
import { EqualHeight, EqualHeightElement } from 'react-equal-height';
import { faEllipsisH} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import LearnImg from "../../assets/images/learn.jpg"
import Routes from "../../routes"
export default () =>{
        const [show, setShow] = useState(false);
        const modalOpen = () => setShow(false);
        const modalShow = () => setShow(true);
        return (
            <EqualHeight>
            <div className="app-main">
            <div className="app-container wide">
            <div className="app-layout-wrapper without-sidebar">
                <Navbar />
                <Modal className="modal-sm" show={show}>
                    <Modal.Body>
                      <Modal.Title>Delete bot</Modal.Title>
                        <span className="d-block">Do you really want to delete this bot</span>
                      <Button variant="secondary" className="btn btn-secondary" onClick={modalOpen}>
                        Cancel
                      </Button>
                      <Button className="btn btn-primary mr-2" onClick={modalOpen}>
                        Ok
                      </Button>
                    </Modal.Body>
                </Modal>
                <div className="app-layout plr-0">
                    <Container fluid>
                        <Row className="bot-wrap">
                            <Col md={2}>
                                <div className="botslist-type">
                                <Dropdown>
                                    <Dropdown.Toggle as="a">
                                        <FontAwesomeIcon icon={faEllipsisH} />
                                    </Dropdown.Toggle> 
                                    <Dropdown.Menu className="dropdown-menu-right">
                                        <div class="up-arrow"></div>
                                        <Dropdown.Item>Setting</Dropdown.Item>
                                        <Dropdown.Item onClick={modalShow}>Delete</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                    <a href={Routes.CustomAbsa.path}>
                                        <div className="bot-name-wrap">
                                            <div className="bot-name">FG</div>
                                        </div>
                                        <h6 className="botslist-title">FGLI</h6>
                                        <EqualHeightElement name="decs">
                                            <p className="botslist-desc">Menu-based chatbots are tree hierarchies presented to
                                            the user in the form of buttons.</p>
                                        </EqualHeightElement>
                                        <div className="bot-channel-list">
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-whatsapp"></i>
                                                </div>
                                            </div>
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-globe"></i>
                                                </div>
                                            </div>
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-skype"></i>
                                                </div>
                                            </div>
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <img src={LearnImg} alt="learn image"/>
                                                </div>
                                            </div>
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <div className="value">+<span>2</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </Col>
                            <Col md={2}>
                                <div className="botslist-type">
                                    <Dropdown>
                                        <Dropdown.Toggle as="a">
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </Dropdown.Toggle> 
                                        <Dropdown.Menu className="dropdown-menu-right">
                                            <div class="up-arrow"></div>
                                            <Dropdown.Item>Setting</Dropdown.Item>
                                            <Dropdown.Item onClick={modalShow}>Delete</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <a href={Routes.CustomAbsa.path}>
                                        <div className="bot-name-wrap">
                                            <div className="bot-name">FG</div>
                                        </div>
                                        <h6 className="botslist-title">BMS</h6>
                                        <EqualHeightElement name="decs">
                                            <p className="botslist-desc">Researching and designing a chatbot to answer the commonly
                                            asked questions.</p>
                                        </EqualHeightElement>
                                        <div className="bot-channel-list">
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-whatsapp"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </Col>
                            <Col md={2}>
                                <div className="botslist-type">
                                    <Dropdown>
                                        <Dropdown.Toggle as="a">
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </Dropdown.Toggle> 
                                        <Dropdown.Menu className="dropdown-menu-right">
                                            <div class="up-arrow"></div>
                                            <Dropdown.Item>Setting</Dropdown.Item>
                                            <Dropdown.Item onClick={modalShow}>Delete</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <a  href={Routes.CustomAbsa.path}>
                                        <div className="bot-name-wrap">
                                            <div className="bot-name">AB</div>
                                        </div>
                                        <h6 className="botslist-title">ABSA</h6>
                                        <EqualHeightElement name="decs">
                                            <p className="botslist-desc">Researching and designing a chatbot to answer the commonly
                                            asked questions.</p>
                                        </EqualHeightElement>
                                        <div className="bot-channel-list">
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-whatsapp"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>        
                            </Col>
                            <Col md={2}>
                                <div className="botslist-type">
                                    <Dropdown>
                                        <Dropdown.Toggle as="a">
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </Dropdown.Toggle> 
                                        <Dropdown.Menu className="dropdown-menu-right">
                                            <div class="up-arrow"></div>
                                            <Dropdown.Item>Setting</Dropdown.Item>
                                            <Dropdown.Item onClick={modalShow}>Delete</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <a href={Routes.CustomAbsa.path}>
                                        <div className="bot-name-wrap">
                                            <div className="bot-name">HE</div>
                                        </div>
                                        <h6 className="botslist-title">Helpify</h6>
                                        <EqualHeightElement name="decs">
                                        <p className="botslist-desc">Categorise the questions.</p>
                                        </EqualHeightElement>
                                        <div className="bot-channel-list">
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-whatsapp"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div> 
                            </Col>
                            <Col md={2}>
                                <div className="botslist-type">
                                    <Dropdown>
                                        <Dropdown.Toggle as="a">
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </Dropdown.Toggle> 
                                        <Dropdown.Menu className="dropdown-menu-right">
                                            <div class="up-arrow"></div>
                                            <Dropdown.Item>Setting</Dropdown.Item>
                                            <Dropdown.Item onClick={modalShow}>Delete</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <a href={Routes.CustomAbsa.path}>
                                        <div className="bot-name-wrap">
                                            <div className="bot-name">FGL</div>
                                        </div>
                                        <h6 className="botslist-title">FGLI</h6>
                                        <EqualHeightElement name="decs">
                                            <p className="botslist-desc">QnA Maker is service that easily creates a natural
                                            conversational layer over your data.</p>
                                        </EqualHeightElement>
                                        <div className="bot-channel-list">
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-whatsapp"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>             
                            </Col>
                            <Col md={2}>
                                <div className="botslist-type"> 
                                    <Dropdown>
                                        <Dropdown.Toggle as="a">
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </Dropdown.Toggle> 
                                        <Dropdown.Menu className="dropdown-menu-right">
                                            <div class="up-arrow"></div>
                                            <Dropdown.Item>Setting</Dropdown.Item>
                                            <Dropdown.Item onClick={modalShow}>Delete</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <a href={Routes.CustomAbsa.path}>
                                        <div className="bot-name-wrap">
                                            <div className="bot-name">BB</div>
                                        </div>
                                        <h6 className="botslist-title">Banking Bot</h6>
                                        <EqualHeightElement name="decs">
                                        <p className="botslist-desc">Back-and-forth chat that makes conversations natural, but
                                            doesn’t directly relate to the user’s goal.</p>
                                        </EqualHeightElement>
                                        <div className="bot-channel-list">
                                            <div className="b-channetitem">
                                                <div className="channel-item">
                                                    <i className="fa fa-whatsapp"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>            
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        </div>
        </div>     
        </EqualHeight>

        )
}
