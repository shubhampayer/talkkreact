import React, { Component } from 'react';
import Navbar from '../components/header2';
import Sidebar from '../components/sidebar';
import DashboardStats from "./dashboard-stat"
export default class dashboard extends Component {
    render() {
        return (
            <div>
                <div class="app-main">
                    <div id="appDashboard" class="app-container wide">
                      <Sidebar />
                      <div class="app-layout-wrapper">
                        <Navbar />
                        <div class="app-layout">
                          <div class="text-right mb-15">
                            <button type="button" class="btn o-btn-lgray open-filter">
                              <i class="fa fa-filter"></i>
                            </button>
                          </div>
                          <DashboardStats />
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        )
    }
}
