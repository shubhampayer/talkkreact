import React, { Component } from 'react'
import {Row,Col} from 'react-bootstrap';
import { CircularProgressbar,buildStyles } from 'react-circular-progressbar';
import LineChart from 'react-linechart';
import ReactApexChart from 'react-apexcharts';


export default class dashboardstat extends Component {
    constructor(props) {
        super(props);

        this.state = {
          series: [{
            name: 'Users',
            data: [4, 3, 7, 9, 8, 2, 7, 5]
          }],
          options: {
            chart: {
              parentHeightOffset: 15,
              height: '200px',
              type: 'line',
              toolbar: {
                show: false,
              },
            },
            forecastDataPoints: {
                show: false,
            },
            tooltip: {
                enabled: false,
            },
            markers: {
                size: 5,
                colors: undefined,
                strokeColors: '#fff',
                strokeWidth: 2,
                strokeOpacity: 0.9,
                strokeDashArray: 0,
                fillOpacity: 1,
                discrete: [],
                shape: "circle",
                radius: 2,
                hover: {
                  size: 6,
                  sizeOffset: 3
                }
            },
            legend: {
                show: false
            },
            stroke: {
              width: 3,
              curve: 'smooth'
            },
            title: {
                show: false,
            },
            fill: {
                type: 'gradient',
                gradient: {
                  shade: 'dark',
                  gradientToColors: [ '#FE3C67'],
                  shadeIntensity: 1,
                  type: 'horizontal',
                  opacityFrom: 1,
                  opacityTo: 1,
                  stops: [0, 100, 100, 100]
                }, 
            },
            xaxis: {
                show: false,
                labels: {
                    show: false,
                },
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
                title: {
                    show: false,
                },
            },
            yaxis: {
                show: false,
            },
            grid: {
                show: false,
            },            
          },
        };
    }
    render() {
        const data = [
            {									
                color: "#01AAAD", 
                points: [{x: 1, y: 2}, {x: 5, y: 25}, {x: 10, y: 3},{x: 15, y: 15}, {x: 18, y: 12}] 
            }
        ];
        return (
            <div className="usage-stats-wizard">
                <Row>
                    <Col md={3} sm={6}>
                        <div className="card click-stats">
                            <div className="stats-info-box">
                                <div className="top-info info">
                                    <span className="stats-title">Total Users</span>
                                    <span className="stats-count">34000</span>
                                    <span className="stats-percent">20%</span>
                                    <div className="stats-percent-pie">
                                        <div className="click-stats-percent percent-chart" data-percent="20">
                                            <CircularProgressbar value={20} styles={buildStyles({
                                              pathColor: `#fe3965`,
                                              textColor: '#f88',
                                              trailColor: 'rgba(255,255,255,.4)',
                                              backgroundColor: '#3e98c7',
                                            })}/>
                                            <div className="percentage"><span className="percent">20</span><span className="persent-syb">%</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="bottom-info info">
                                      <ReactApexChart options={this.state.options} series={this.state.series} type="line" height={350} />
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col md={3} sm={6}>
                        <div className="card guest-stats">
                            <div className="stats-info-box">
                                <div className="top-info info">
                                    <span className="stats-title">Guest</span>
                                    <span className="stats-count">11103</span>
                                    <span className="stats-percent">20%</span>
                                    <div class="stats-percent-pie">
                                        <div class="guest-stats-percent percent-chart" data-percent="20">
                                            <CircularProgressbar value={20} styles={buildStyles({
                                              pathColor: `#0ac383`,
                                              textColor: '#f88',
                                              trailColor: 'rgba(255,255,255,.4)',
                                              backgroundColor: '#3e98c7',
                                            })}/>
                                            <div class="percentage"><span class="percent">20</span><span class="persent-syb">%</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-info info">
                                    <LineChart 
                                        width={301}
                                        height={150}
                                        hideXAxis= {true}
                                        hideYAxis= {true}
                                        data={data}
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col md={3} sm={6}>
                        <div class="card members-stats">
                            <div class="stats-info-box">
                                <div class="top-info info">
                                    <span class="stats-title">Customers</span>
                                    <span class="stats-count">22897</span>
                                    <span class="stats-percent">20%</span>
                                    <div class="stats-percent-pie">
                                        <div class="members-stats-percent percent-chart" data-percent="20">
                                            <CircularProgressbar value={20} styles={buildStyles({
                                              pathColor: `#fe5e71`,
                                              textColor: '#f88',
                                              trailColor: 'rgba(255,255,255,.4)',
                                              backgroundColor: '#3e98c7',
                                            })}/>
                                            <div class="percentage"><span class="percent">20</span><span class="persent-syb">%</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-info info">
                                    <LineChart 
                                        width={301}
                                        height={150}
                                        hideXAxis= {true}
                                        hideYAxis= {true}
                                        data={data}
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col md={3} sm={6}>
                        <div className="card activeUsers-stats">
                            <div className="stats-info-box">
                                <div className="top-info info">
                                    <span className="stats-title">Unique Users</span>
                                    <span className="stats-count">10950</span>
                                    <span className="stats-percent">20%</span>
                                    <div className="stats-percent-pie">
                                        <div className="activeUsers-stats-percent percent-chart" data-percent="20">
                                            <CircularProgressbar value={20} styles={buildStyles({
                                              pathColor: `#01aaad`,
                                              textColor: '#f88',
                                              trailColor: 'rgba(255,255,255,.4)',
                                              backgroundColor: '#3e98c7',
                                            })}/>
                                            <div className="percentage"><span className="percent">20</span><span className="persent-syb">%</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="bottom-info info">
                                    <LineChart 
                                        width={301}
                                        height={150}
                                        hideXAxis= {true}
                                        hideYAxis= {true}
                                        data={data}
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>    
        )
    }
}
