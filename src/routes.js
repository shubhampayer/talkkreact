
export const Routes = {
    // pages
    CompanyCode: { path:'/' },
    Login: { path:'/login' },
    Forgot: { path:'/forgot-password' },
    Botlist: { path:'/admin/bot-list' },
    CustomAbsa: { path:'/admin/custom-absa' },
    Dashboard: { path:'/admin/dashboard' },
    // docs

    // components
};
export default Routes;
