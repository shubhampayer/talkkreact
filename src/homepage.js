import './assets/scss/style.scss';
import { BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Login from "./views/login/login-page/login";
import CompanyCode from "./views/login/company-code";
import Forgot from "./views/login/forgot-pass";
import Botlist from "./views/bot/bot-list";
import NotFound from "./views/error/not-found";
import CustomAbsa from "./views/custom_absa/custom-absa";
import Dashboard from "./views/dashboard/dashboard";
import Routes from "./routes";
function Auth() {
  return (
    <Router>
      <Switch>
        <Route exact path={Routes.CompanyCode.path} component={CompanyCode} />
        <Route exact path={Routes.Login.path} component={Login} />
        <Route exact path={Routes.Forgot.path} component={Forgot} />
        <Route exact path={Routes.Botlist.path} component={Botlist} />
        <Route exact path={Routes.CustomAbsa.path} component={CustomAbsa} />
        <Route exact path={Routes.Dashboard.path} component={Dashboard} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
}
export default Auth;

